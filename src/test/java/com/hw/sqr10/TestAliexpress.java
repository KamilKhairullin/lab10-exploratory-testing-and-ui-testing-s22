package com.hw.sqr10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.util.concurrent.TimeUnit;
public class TestAliexpress {
    WebDriver driver = new FirefoxDriver();
    final String url = "https://aliexpress.ru";

    @Test
    public void testAliexpress() {
        driver.get(url);
        WebElement profileButton = driver.findElement(By.xpath("/html/body/div/div/div[1]/div/span/div[4]/div[2]/div[1]/a/span"));
        profileButton.click();
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);

        WebElement login = driver.findElement(By.xpath("//*[@id=\'fm-login-id\']"));
        login.click();
        login.sendKeys("Login");
        Assert.assertEquals("Login", login.getAttribute("value"));

        WebElement password = driver.findElement(By.xpath("//*[@id=\'fm-login-password\']"));
        password.click();
        password.sendKeys("Password");
        Assert.assertEquals("Password", password.getAttribute("value"));
        driver.quit();
    }
}

